CREATE OR ALTER VIEW view_with_check AS
SELECT name, language FROM film
WHERE language IS NOT NULL
WITH CHECK OPTION
GO

SELECT * FROM view_with_check