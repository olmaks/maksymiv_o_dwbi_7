DROP TRIGGER IF EXISTS update_date
GO
CREATE TRIGGER update_date ON film
AFTER UPDATE 
AS
BEGIN
	UPDATE film
	SET film.updated_date = GETDATE()
END
GO

UPDATE film SET name='Timeless' WHERE id=1
GO

SELECT * FROM film
