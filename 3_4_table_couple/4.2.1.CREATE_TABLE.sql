DROP TABLE IF EXISTS student, groups 
GO

CREATE TABLE groups (
	group_id tinyint NOT NULL PRIMARY KEY,
	name char(5) NOT NULL
)

CREATE TABLE student (
	id INT NOT NULL PRIMARY KEY,
	name char(30) NOT NULL,
	surname char(30) NOT NULL,
	lastname char(30) NOT NULL, 
	biography text,
	birthday date,
	rating decimal,
	money decimal,
	group_id tinyint REFERENCES groups,
	photo varbinary,
)

CREATE TABLE abbiturient (
	id INT NOT NULL PRIMARY KEY,
	name char(30) NOT NULL,
	surname char(30) NOT NULL,
	lastname char(30) NOT NULL, 
	biography text,
	birthday date,
	rating decimal,
	money decimal,
	group_id tinyint REFERENCES groups,
	photo varbinary,
	operation_type char(1) NOT NULL,
	operation_date date DEFAULT GETDATE(),
)
