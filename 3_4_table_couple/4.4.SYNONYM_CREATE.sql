USE education
GO

CREATE SCHEMA O_MAKSYMIV
GO

CREATE SYNONYM dbo.filmtable for O_M_module_3.dbo.film
CREATE SYNONYM dbo.abbit for O_M_module_3.dbo.abbiturient

SELECT name,year FROM filmtable WHERE language IS NULL
SELECT * FROM filmtable WHERE country = 'USA'
SELECT COUNT (*) FROM filmtable
SELECT * FROM abbit