CREATE OR ALTER TRIGGER update_some ON student
AFTER UPDATE
AS
DECLARE @surname char(20)
SELECT @surname=surname FROM student
IF @surname = 'admin'
BEGIN
	PRINT'You cannot be admin!'
	ROLLBACK TRANSACTION
END

CREATE OR ALTER TRIGGER delete_some ON student
FOR DELETE
AS
SELECT COUNT(id) FROM student 
GO

CREATE OR ALTER TRIGGER inserted_some ON student
AFTER INSERT
AS
PRINT 'INSERTED DATE:'
PRINT GETDATE()
GO

SELECT * FROM student
