CREATE OR ALTER VIEW film_view
(
	id,
	name,
	inserted_date,
	updated_date
)
AS
SELECT id, name, inserted_date, updated_date FROM film
GO

CREATE OR ALTER VIEW statistic_view
(
	id,
	number_buys
)
AS
SELECT id, number_buys FROM statistic
GO

SELECT * FROM film_view
SELECT * FROM statistic_view