INSERT INTO [employee] VALUES 
('Maksymiv', 'Oleksii', NULL, '13111992', 'KS66326', 10, '2001-01-01', 'post1', 1),
('Maksymiv', 'Natalia', NULL, '26021995', 'KS66300', 9, '1992-11-13', 'post2', 2),
('Golovatiy', 'Roman', NULL, '12021900', 'KS66310', 8, '1995-12-11', 'post3', 3),
('Jyk', 'Maksym', NULL, '26021905', 'KB66300', 7, '1993-11-13', 'post4', 4)

INSERT INTO [medicine] VALUES 
('m_name1', 'MA-123-12', 1, 1, 1),
('m_name2', 'AP-123-12', 1, 1, 1),
('m_name1', 'BC-123-12', 1, 1, 1),
('m_name1', 'BC-123-1A', 1, 1, 1)

INSERT INTO [medicine_zone] VALUES 
(1, 1),
(2, 2),
(3, 3),
(4, 4)

INSERT INTO [medicine] VALUES 
('m_name1', 'MA-123-12', 1, 1, 1),
('m_name2', 'AP-123-12', 1, 1, 1),
('m_name1', 'BC-123-12', 1, 1, 1),
('m_name1', 'BC-123-1A', 1, 1, 1)

INSERT INTO [pharmacy] VALUES 
('phar_name1', '78', 'www.a.com', '10:10', 1, 1, 'Kylpark'),
('phar_name2', '22', 'www.b.com', '11:20', 0, 0, 'Bandery'),
('phar_name3', '7', 'www.c.com', '12:30', 0, 1, 'Kylpark'),
('phar_name4', '11', 'www.d.com', '13:40', 1, 0, 'Naykova')

INSERT INTO [pharmacy_medicine] VALUES 
(1, 1),
(2, 2),
(3, 3),
(4, 4)

INSERT INTO [post] VALUES 
('post1'),
('post2'),
('post3'),
('post4')

INSERT INTO [street] VALUES 
('Kylpark'),
('Bandery'),
('Naykova')

INSERT INTO [zone] VALUES 
('zone1'),
('zone2'),
('zone3')