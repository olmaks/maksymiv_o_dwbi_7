DECLARE @str_date varchar(20)
SET @str_date = CONVERT(varchar(20), FORMAT(GETDATE() , 'HH_mm_ss'))

DECLARE @rand_number INT
SET @rand_number = CAST(ROUND(RAND(),0) AS BIT)

DECLARE @SQLString nvarchar(max)

--tables columns
DECLARE @id INT

DECLARE date_tables CURSOR
	FOR SELECT id FROM employee
OPEN date_tables
FETCH NEXT FROM date_tables INTO @id
SET @SQLString = 'CREATE TABLE employee' + @str_date + '_1(
	id INT,
	surname VARCHAR(30),
	name VARCHAR(30),
	midle_name VARCHAR(30), 
	identity_number VARCHAR(30), 
	passport VARCHAR(30), 
	experience DECIMAL(10, 1), 
	birthday DATE, 
	post VARCHAR(15), 
	pharmacy_id INT)'
EXECUTE(@SQLString)
SET @SQLString = 'CREATE TABLE employee' + @str_date + '_2(
	id INT,
	surname VARCHAR(30),
	name VARCHAR(30),
	midle_name VARCHAR(30), 
	identity_number VARCHAR(30), 
	passport VARCHAR(30), 
	experience DECIMAL(10, 1), 
	birthday DATE, 
	post VARCHAR(15), 
	pharmacy_id INT)'
EXECUTE(@SQLString)
IF @rand_number = 1
	EXECUTE('INSERT INTO employee' + @str_date + '_2 SELECT * FROM employee')
ELSE
	EXECUTE('INSERT INTO employee' + @str_date + '_1 SELECT * FROM employee')

CLOSE date_tables
DEALLOCATE date_tables