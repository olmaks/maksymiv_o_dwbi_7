CREATE OR ALTER TRIGGER check_name_insert ON [employee]
INSTEAD OF INSERT
AS
INSERT INTO [employee](surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id) 
SELECT surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id
FROM INSERTED WHERE identity_number NOT LIKE '%00'
GO

CREATE OR ALTER TRIGGER medic_format ON [medicine]
INSTEAD OF INSERT
AS
INSERT INTO [medicine](name, ministry_code, recipe, narcotic, psychotropic) 
SELECT name, ministry_code, recipe, narcotic, psychotropic
FROM INSERTED WHERE ministry_code LIKE '[^MP][^MP]-[0-9][0-9][0-9]-[0-9][0-9]'
GO

CREATE OR ALTER TRIGGER forbid_post_change ON [post]
AFTER UPDATE, DELETE
AS
BEGIN
PRINT 'You cannot delete or update something here'
ROLLBACK TRANSACTION
END
GO