DECLARE @Surname varchar(20), @name varchar(20)
DECLARE @SQLString nvarchar(max)
DECLARE @column_num int

DECLARE create_tables CURSOR
	FOR SELECT surname, name FROM employee
OPEN create_tables
FETCH NEXT FROM create_tables into @Surname, @name
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @column_num = floor(rand()*10) + 1
	SET @SQLString = 'CREATE TABLE ' + @Surname + '_' + @name + ' ('
	WHILE @column_num > 0
	BEGIN
		SET @SQLString = @SQLString + 'columnname' + CONVERT(varchar(20), @column_num) + ' VARCHAR(20),'
		SET @column_num = @column_num - 1
	END
	SET @SQLString = LEFT(@SQLString, LEN(@SQLString) - 1) + ')'
	EXECUTE(@SQLString)
	FETCH NEXT FROM create_tables INTO @Surname, @name
END

CLOSE create_tables
DEALLOCATE create_tables

SELECT * FROM Jyk_Maksym