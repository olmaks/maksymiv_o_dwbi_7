USE O_MAKSYMIV
GO

CREATE OR ALTER TRIGGER publisher_book_count
ON Books
AFTER INSERT, UPDATE
AS
UPDATE Publishers
SET book_amount = (SELECT Count(Publisher_id)
FROM Books WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)) WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)
UPDATE Publishers 
SET issue_amount = (SELECT Sum(issue)
FROM Books WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)) WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)
UPDATE Publishers 
SET total_edition = book_amount + issue_amount WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)
UPDATE Publishers 
SET updated = GETDATE() WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)
UPDATE Publishers 
SET updated_by = SYSTEM_USER WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)
GO

CREATE OR ALTER TRIGGER publisher_count_delete
ON Books
AFTER DELETE
AS
UPDATE Publishers
SET book_amount = (SELECT Count(Publisher_id)
FROM Books WHERE Publisher_id = (SELECT Publisher_id FROM DELETED)) WHERE Publisher_id = (SELECT Publisher_id FROM DELETED)
UPDATE Publishers 
SET issue_amount = (SELECT Sum(issue)
FROM Books WHERE Publisher_id = (SELECT Publisher_id FROM DELETED)) WHERE Publisher_id = (SELECT Publisher_id FROM DELETED)
UPDATE Publishers 
SET total_edition = book_amount + issue_amount WHERE Publisher_id = (SELECT Publisher_id FROM DELETED)
GO

DROP TRIGGER publisher_book_update
ON Books
AFTER UPDATE
AS
UPDATE Publishers
SET book_amount = (SELECT Count(Publisher_id)
FROM Books WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)) WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)
UPDATE Publishers 
SET issue_amount = (SELECT Sum(issue)
FROM Books WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)) WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)
UPDATE Publishers 
SET total_edition = book_amount + issue_amount WHERE Publisher_id = (SELECT Publisher_id FROM INSERTED)
GO


--SELECT * FROM Books
--DELETE FROM Authors_log WHERE operation_id > 0
--DELETE TOP(1) FROM Books
--INSERT INTO Books(ISBN, Publisher_id, issue, URL) VALUES('isbn-15', 1, 15, 'www.some15.come')
--UPDATE Books SET issue = 22 WHERE Publisher_id = 1
--SELECT * FROM Publishers
--INSERT INTO Publishers(Publisher_id, Name) VALUES(3, 'Ababa3')
