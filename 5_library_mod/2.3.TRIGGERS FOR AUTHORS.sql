CREATE OR ALTER TRIGGER author_book_count
ON BooksAuthors
AFTER INSERT, UPDATE
AS
UPDATE Authors
SET book_amount = (SELECT COUNT(ISBN) FROM Books WHERE ISBN IN (SELECT ISBN FROM BooksAuthors WHERE Author_id = (SELECT Author_id FROM INSERTED)))
UPDATE Authors
SET issue_amount = (SELECT SUM(issue) FROM Books WHERE ISBN IN (SELECT ISBN FROM BooksAuthors WHERE Author_id = (SELECT Author_id FROM INSERTED)))
UPDATE Authors
SET total_edition = book_amount + issue_amount
UPDATE Authors
SET updated = GETDATE()
UPDATE Authors
SET updated_by = SYSTEM_USER
GO

CREATE OR ALTER TRIGGER author_book_delete
ON BooksAuthors
AFTER DELETE
AS
UPDATE Authors
SET book_amount = (SELECT COUNT(ISBN) FROM Books WHERE ISBN IN (SELECT ISBN FROM BooksAuthors WHERE Author_id = (SELECT Author_id FROM DELETED)))
UPDATE Authors
SET issue_amount = (SELECT SUM(issue) FROM Books WHERE ISBN IN (SELECT ISBN FROM BooksAuthors WHERE Author_id = (SELECT Author_id FROM DELETED)))
UPDATE Authors
SET total_edition = book_amount + issue_amount
UPDATE Authors
SET updated = GETDATE()
UPDATE Authors
SET updated_by = SYSTEM_USER
