USE O_MAKSYMIV
GO

CREATE OR ALTER TRIGGER author_insert
ON Authors
AFTER INSERT
AS
INSERT INTO Authors_log(
	Author_Id_new, Name_new, URL_new, operation_type, 
	Author_Id_old, Name_old, URL_old,
	book_amount_new, issue_amount_new, total_edition_new,
	book_amount_old, issue_amount_old, total_edition_old)
SELECT Author_Id, Name, URL, 'I', NULL, NULL, NULL, book_amount, issue_amount, total_edition, NULL, NULL, NULL
FROM INSERTED
GO

CREATE OR ALTER TRIGGER author_delete
ON Authors
AFTER DELETE
AS
INSERT INTO Authors_log(
	Author_Id_old, Name_old, URL_old, operation_type, 
	Author_Id_new, Name_new, URL_new, 
	book_amount_new, issue_amount_new, total_edition_new,
	book_amount_old, issue_amount_old, total_edition_old
	) 
SELECT Author_Id, Name, URL, 'D', NULL, NULL, NULL, NULL, NULL, NULL, book_amount, issue_amount, total_edition
FROM DELETED
GO

CREATE OR ALTER TRIGGER author_update
ON Authors
AFTER UPDATE
AS
INSERT INTO Authors_log(
	Author_Id_new, Name_new, URL_new,
	book_amount_new, issue_amount_new, total_edition_new,
	Author_Id_old, Name_old, URL_old, operation_type,
	book_amount_old, issue_amount_old, total_edition_old
	)
SELECT inserted.Author_Id, inserted.Name, inserted.URL, inserted.book_amount, inserted.issue_amount, inserted.total_edition,
deleted.Author_Id, deleted.Name, deleted.URL, 'U', deleted.book_amount, deleted.issue_amount, deleted.total_edition
FROM Authors
inner join inserted on Authors.Author_id = inserted.Author_id
inner join deleted on Authors.Author_id = deleted.Author_id
GO


--SELECT * FROM Authors_log
--DELETE FROM Authors_log WHERE operation_id > 0
--DELETE FROM Authors WHERE Author_id = 1
--INSERT INTO Authors(Author_id, Name) VALUES(1, 'Christofer Nolan')
--UPDATE [dbo].[Authors] SET Name = 'Bolan' WHERE Author_id = 1
