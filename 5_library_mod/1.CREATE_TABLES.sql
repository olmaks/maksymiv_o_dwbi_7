USE O_MAKSYMIV
GO

DROP TABLE IF EXISTS Authors
GO
CREATE TABLE Authors (
	Author_id INT NOT NULL PRIMARY KEY,
	Name varchar(40) NOT NULL UNIQUE,
	URL varchar(40) NOT NULL DEFAULT 'www.author_name.com',
	inserted date NOT NULL DEFAULT getdate(),
	inserted_by varchar(40) NOT NULL DEFAULT system_user,
	updated date,
	updated_by varchar(40),
	birthday date,
	book_amount INT NOT NULL DEFAULT 0 CHECK(book_amount >= 0),
	issue_amount INT NOT NULL DEFAULT 0 CHECK(issue_amount >= 0),
	total_edition INT NOT NULL DEFAULT 0 CHECK(total_edition >= 0)
)

DROP TABLE IF EXISTS Publishers
GO
CREATE TABLE Publishers (
	Publisher_id INT NOT NULL PRIMARY KEY,
	Name varchar(40) NOT NULL UNIQUE,
	URL varchar(40) NOT NULL DEFAULT 'www.publisher.name.com',
	inserted date NOT NULL DEFAULT getdate(),
	inserted_by varchar(40) NOT NULL DEFAULT system_user,
	updated date,
	updated_by varchar(40),
	created date DEFAULT '1900-01-01' NOT NULL,
	country varchar(40) NOT NULL DEFAULT 'USA',
	city varchar(40) NOT NULL DEFAULT 'NY',
	book_amount INT NOT NULL DEFAULT 0 CHECK(book_amount >= 0),
	issue_amount INT NOT NULL DEFAULT 0 CHECK(issue_amount >= 0),
	total_edition INT NOT NULL DEFAULT 0 CHECK(total_edition >= 0)
)

DROP TABLE IF EXISTS Authors_log
GO
CREATE TABLE Authors_log (
	operation_id INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Author_Id_new INT,
	Name_new varchar(40),
	URL_new varchar(40),
	Author_Id_old INT,
	Name_old varchar(40),
	URL_old varchar(40),
	operation_type varchar(40) NOT NULL CHECK(operation_type IN ('I','D','U')) DEFAULT 'I',
	operation_datetime date NOT NULL DEFAULT getdate(),
	book_amount_old INT,
	issue_amount_old INT,
	total_edition_old INT,
	book_amount_new INT,
	issue_amount_new INT,
	total_edition_new INT
)

DROP TABLE IF EXISTS Books
GO
CREATE TABLE Books (
	ISBN varchar(40) NOT NULL UNIQUE,
	Publisher_id INT NOT NULL FOREIGN KEY REFERENCES Publishers(Publisher_id),
	URL varchar(40) NOT NULL UNIQUE,
	Price INT NOT NULL DEFAULT 0 CHECK(Price >= 0),
	inserted date NOT NULL DEFAULT getdate(),
	inserted_by varchar(40) NOT NULL DEFAULT system_user,
	updated date,
	updated_by varchar(40),
	title varchar(40) NOT NULL DEFAULT 'Title',
	edition INT NOT NULL DEFAULT 1 CHECK(edition >= 1),
	published date,
	issue INT PRIMARY KEY
)

DROP TABLE IF EXISTS BooksAuthors
GO
CREATE TABLE BooksAuthors (
	BooksAuthors_id INT NOT NULL PRIMARY KEY DEFAULT 1 CHECK(BooksAuthors_id >= 1),
	ISBN varchar(40) NOT NULL FOREIGN KEY REFERENCES Books(ISBN),
	Author_id INT NOT NULL FOREIGN KEY REFERENCES Authors(Author_id),
	Seq_No INT NOT NULL DEFAULT 1 CHECK(Seq_No >= 1),
	inserted date NOT NULL DEFAULT getdate(),
	inserted_by varchar(40) NOT NULL DEFAULT system_user,
	updated date,
	updated_by varchar(40)
)