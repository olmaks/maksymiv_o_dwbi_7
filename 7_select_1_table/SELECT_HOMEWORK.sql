USE [labor_sql]
GO
--1
SELECT maker, type FROM product ORDER BY maker DESC

--2
SELECT product.model, laptop.ram, laptop.screen, laptop.price FROM Laptop
INNER JOIN product on product.model = laptop.model AND laptop.price > 1000 ORDER BY ram DESC, price ASC

--3
SELECT * FROM printer WHERE color = 'y' ORDER BY price DESC

--4
SELECT model, speed, hd, cd, price FROM pc WHERE (cd = '12x' or cd = '24x') AND price < 600
ORDER BY speed ASC

--5
SELECT name, class FROM ships ORDER BY name ASC

--6
SELECT * FROM PC WHERE speed >= 500 AND price < 800 ORDER BY price DESC

--7
SELECT * FROM printer WHERE price < 300 AND type = 'Matrix'

--8
SELECT model, speed FROM PC WHERE price >= 400 AND price <= 600 ORDER BY hd ASC

--9
SELECT model, speed, hd, price FROM laptop WHERE screen >= 12 ORDER BY price DESC

--10
SELECT model, type, price FROM printer WHERE price < 300 ORDER BY type DESC

--11
SELECT model, ram, price FROM laptop WHERE RAM = 64 ORDER BY price ASC

--12
SELECT model, ram, price FROM laptop WHERE RAM > 64 ORDER BY hd ASC

--13
SELECT model, speed, price FROM PC WHERE speed BETWEEN 500 AND 750 ORDER BY hd DESC

--14
SELECT * FROM Outcome_o WHERE out > 2000 ORDER BY date DESC

--15
SELECT * FROM Income_o WHERE inc BETWEEN 5000 AND 10000 ORDER BY inc ASC

--16
SELECT * FROM Income WHERE point = 1 ORDER BY inc ASC

--17
SELECT * FROM outcome WHERE point = 2 ORDER BY out DESC

--18
SELECT * FROM classes WHERE country = 'Japan' ORDER BY type DESC

--19
SELECT name, launched FROM ships WHERE launched BETWEEN 1920 AND 1942 ORDER BY launched DESC

--20
SELECT ship, battle, result FROM outcomes WHERE result <> 'sunk' ORDER BY ship DESC

--21
SELECT ship, battle, result FROM outcomes WHERE result = 'sunk' ORDER BY ship DESC

--22
SELECT class, displacement FROM classes WHERE displacement < 40000 ORDER BY type ASC

--23
SELECT trip_no, town_from, town_to FROM trip WHERE town_from = 'London' or town_to = 'London' ORDER BY time_out ASC

--24
SELECT trip_no, plane, town_from, town_to FROM trip WHERE plane = 'TU-134' ORDER BY time_out DESC

--25
SELECT trip_no, plane, town_from, town_to FROM trip WHERE plane <> 'IL-86' ORDER BY plane ASC

--26
SELECT trip_no, town_from, town_to FROM trip WHERE town_from <> 'Rostov' AND town_to <> 'Rostov' ORDER BY plane ASC

--27
SELECT * FROM pc WHERE model LIKE '%11%' or model LIKE '%1%1%'

--28
SELECT * FROM Outcome WHERE DATEPART(mm, date) = 03

--29
SELECT * FROM Outcome_o WHERE DATEPART(dd, date) = 14

--30
SELECT name FROM Ships WHERE name LIKE 'W%n'

--31
SELECT name FROM Ships WHERE name LIKE '%ee%' OR name LIKE '%e%e%'

--32
SELECT name, launched FROM Ships WHERE name LIKE '%[^a]'

--33
SELECT * FROM battles WHERE name LIKE '% %[^c]'

--34
SELECT * FROM Trip WHERE DATEPART(hh, time_out) BETWEEN 12 AND 17

--35
SELECT * FROM Trip WHERE DATEPART(hh, time_in) BETWEEN 17 AND 23

--36
SELECT * FROM Trip WHERE (DATEPART(hh, time_in) BETWEEN 21 AND 24) OR DATEPART(hh, time_in) BETWEEN 00 AND 10

--37
SELECT date, place FROM Pass_in_trip WHERE place LIKE '1%'

--38
SELECT * FROM Pass_in_trip WHERE place LIKE '%c'

--39
SELECT name FROM Passenger WHERE name LIKE '% C%'

--40
SELECT name FROM Passenger WHERE name LIKE '% [^J]%'

--41
SELECT '������� ���� =' + cast(AVG(price) as varchar(20)) FROM laptop

--42
SELECT '������ =' + cast(model as varchar(20)),  '�������� =' + cast(speed as varchar(20)),
'ram =' + cast(ram as varchar(20)), 'hd =' + cast(hd as varchar(20)), 
'price =' + cast(price as varchar(20)), 'cd =' + cast(cd as varchar(20))
FROM PC

--43
SELECT convert(varchar, date, 102) FROM income

--44
SELECT ship,battle,result,
CASE result
when 'sunk' then '����������'
when 'damaged' then '�����������'
else '�����'
end as name
FROM Outcomes 

--45
SELECT '���:' + SUBSTRING(place,1,1), '����: ' + SUBSTRING(place,2,1) FROM Pass_in_trip

--46
SELECT trip_no, id_comp, plane, CONCAT('from ', town_to, 'to ', town_from) FROM Trip

--47
SELECT LEFT(trip_no,1) + RIGHT(trip_no,1) + LEFT(id_comp,1) + RIGHT(id_comp,1) +
LEFT(plane,1) + RIGHT(plane,1) + LEFT(town_from,1) + RIGHT(town_from,1)
+ LEFT(town_to,1) + RIGHT(town_to,1) + LEFT(time_out,1) + RIGHT(time_out,1) FROM TRIP

--48
SELECT maker, COUNT(model) FROM product GROUP BY maker HAVING COUNT(model) > 2

--49
SELECT DISTINCT town_from, COUNT(town_from) + COUNT(town_to) AS aa FROM trip GROUP BY town_from

--50
SELECT type, COUNT(model) FROM printer GROUP BY type

--51
SELECT cd, COUNT(model) FROM PC GROUP BY cd

--52
SELECT convert(varchar, time_in - time_out, 108) FROM trip

--53
SELECT point, convert(varchar, date, 102) AS 'date', SUM(out) AS 'sum', 
MIN(out) as 'min', MAX(out) as 'max' FROM Outcome 
GROUP BY point, convert(varchar, date, 102) WITH ROLLUP

--54
SELECT trip_no, LEFT(place, 1) AS 'place', COUNT(place) AS 'N_places' 
FROM Pass_in_trip GROUP BY trip_no, LEFT(place, 1) WITH ROLLUP

--55
SELECT count(name) FROM passenger WHERE name LIKE '[SBA]%'