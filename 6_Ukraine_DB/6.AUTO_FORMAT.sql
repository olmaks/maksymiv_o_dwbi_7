USE people_ua_db
GO

/*ALTER INDEX ALL 
  ON dbo.ua_citizen  
REBUILD ;   
GO 
*/

SELECT 
	dbtables.[name] as 'Table',
	dbindexes.[name] as 'Index',
	indexstats.avg_fragmentation_in_percent,
	indexstats.page_count
	FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS indexstats
	INNER JOIN sys.tables dbtables on dbtables.[object_id] = indexstats.[object_id]
	INNER JOIN sys.indexes AS dbindexes ON dbindexes.[object_id] = indexstats.[object_id]
	AND indexstats.index_id = dbindexes.index_id
	WHERE indexstats.database_id = DB_ID()
	ORDER BY indexstats.avg_fragmentation_in_percent desc

/*If Page count is >1000 then
if fragmentation is less than 5% then leave as it is.
if fragmentation is 5% - 30% then reorganize the index.
if fragmentation is  > 30% then rebuild the index.
*/