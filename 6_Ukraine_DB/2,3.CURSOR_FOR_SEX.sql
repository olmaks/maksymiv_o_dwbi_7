USE people_ua_db

DROP TABLE IF EXISTS ua_citizen
GO
CREATE TABLE ua_citizen(id INT, surname varchar(20), name varchar(20), sex varchar(1), birthday datetime)

INSERT INTO ua_citizen(id, surname, sex) SELECT id, surname, sex FROM surname_list_identity

--START CURSOR FROM HERE
DECLARE @name varchar(20)
DECLARE @sex varchar(1)
DECLARE @id int
DECLARE @rand_numb INT
DECLARE @birthday datetime
DECLARE name_insert CURSOR
	FOR SELECT id, sex, name, birthday FROM ua_citizen
OPEN name_insert
FETCH NEXT FROM name_insert into @id, @sex, @name, @birthday
WHILE @@FETCH_STATUS = 0
BEGIN
UPDATE ua_citizen
--SET birthday = FLOOR(RAND()*(2000-1925)+1925) WHERE id = @id
SET birthday = DATEADD(day, ROUND(DATEDIFF(day, '1925-01-01', '2000-12-31') * RAND(CHECKSUM(NEWID())), 0),
    DATEADD(second, CHECKSUM(NEWID()) % 48000, '1925-01-01'))
--main part. CHECK FOR WOMAN
SET @rand_numb = FLOOR(RAND()*((SELECT COUNT(id) FROM name_list_identity)-1)+1)
IF @sex = 'w' 
BEGIN
WHILE (SELECT COUNT(*) FROM (SELECT name FROM name_list_identity WHERE id = @rand_numb AND sex = 'w') a) < 1
	BEGIN
	SET @rand_numb = FLOOR(RAND()*((SELECT COUNT(id) FROM name_list_identity)-1)+1)
	END
UPDATE ua_citizen
SET name = (SELECT name FROM name_list_identity WHERE id = @rand_numb AND sex = 'w')
WHERE @id = id
END
--CHECK FOR NULL NAME
IF @sex is NULL
BEGIN
UPDATE ua_citizen
SET name = (SELECT name FROM name_list_identity WHERE id = @rand_numb)
WHERE @id = id
END
--CHECK FOR MAN
IF @sex = 'm' 
BEGIN
WHILE (SELECT COUNT(*) FROM (SELECT name FROM name_list_identity WHERE id = @rand_numb AND sex = 'm') a) < 1
	BEGIN
	SET @rand_numb = FLOOR(RAND()*((SELECT COUNT(id) FROM name_list_identity)-1)+1)
	END
UPDATE ua_citizen
SET name = (SELECT name FROM name_list_identity WHERE id = @rand_numb AND sex = 'm')
WHERE @id = id
END
FETCH NEXT FROM name_insert into @id, @sex, @name, @birthday
END
CLOSE name_insert
DEALLOCATE name_insert

SELECT * FROM ua_citizen