USE people_ua_db
GO

/*Not-unique clastered index */
CREATE CLUSTERED INDEX cluster_index
	ON people_ua_db.dbo.ua_citizen(id);
GO

DROP INDEX cluster_index
	ON people_ua_db.dbo.ua_citizen
GO

/*Not-unique non-clastered index 
CREATE INDEX noncluster_index
	ON people_ua_db.dbo.ua_citizen(name)
GO
Another way to create the same type of index:
*/
CREATE NONCLUSTERED INDEX noncluster_index
	ON people_ua_db.dbo.ua_citizen(name)
GO

DROP INDEX noncluster_index
	ON people_ua_db.dbo.ua_citizen
GO

/*unique non-clustered index */
CREATE UNIQUE INDEX uniq_index
	ON people_ua_db.dbo.ua_citizen(surname)

DROP INDEX uniq_index
	ON people_ua_db.dbo.ua_citizen
GO

SELECT index_type_desc, index_depth, index_level, page_count, record_count
FROM sys.dm_db_index_physical_stats (DB_ID('people_ua_db'), OBJECT_ID('dbo.ua_citizen'), NULL,
NULL, 'DETAILED')


/*unique clustered index */
CREATE UNIQUE CLUSTERED INDEX uniq_cluster_index
	ON people_ua_db.dbo.ua_citizen(surname)
/*
DROP INDEX uniq_cluster_index
	ON people_ua_db.dbo.ua_citizen
GO
*/
SELECT index_type_desc, index_depth, index_level, page_count, record_count
FROM sys.dm_db_index_physical_stats (DB_ID('people_ua_db'), OBJECT_ID('dbo.ua_citizen'), NULL,
NULL, 'DETAILED')

/*Statistics on*/
SET STATISTICS TIME ON

/*Ctrl+L to look estimated execution plan*/
SELECT * FROM ua_citizen
