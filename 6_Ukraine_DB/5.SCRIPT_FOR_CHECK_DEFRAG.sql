SELECT index_type_desc, avg_fragment_size_in_pages, avg_fragmentation_in_percent
FROM sys.dm_db_index_physical_stats (DB_ID('people_ua_db'), OBJECT_ID('dbo.ua_citizen'), NULL,
NULL, 'DETAILED')

SELECT tbl.name AS table_name, idx.name AS index_name, avg_fragment_size_in_pages, avg_fragmentation_in_percent
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) dm
	INNER JOIN sys.tables tbl ON dm.object_id = tbl.object_id
	INNER JOIN sys.indexes idx ON dm.object_id = idx.object_id AND dm.index_id = idx.index_id