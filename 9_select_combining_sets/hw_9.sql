--1
WITH first_CTE(name, city) AS 
(SELECT name, region_id FROM [geography])
SELECT * FROM first_CTE WHERE city = 2

--2
;WITH computer_CTE(pc_model, pc_speed, pc_price) AS
(SELECT model, speed, price FROM [pc])
SELECT model, speed, price, pc_model, pc_speed, pc_price FROM [laptop]
JOIN computer_CTE ON price = pc_price

--3
;WITH 
geogr_CTE(regionID, name) AS (SELECT region_id, name FROM [geography])
SELECT regionID, place_ID = ROW_NUMBER() OVER (ORDER BY regionID), name FROM geogr_CTE WHERE regionID = 1

--4
;WITH CTE1 AS
(SELECT g.[region_id], g.id place_ID, g.name, Placelevel1=0  FROM [geography] g
WHERE region_id=4
UNION ALL
SELECT g.[region_id], g.id place_ID, g.name, Placelevel1 +1
FROM [geography] g INNER JOIN CTE1 a ON g.[region_id]=place_ID)
SELECT * FROM CTE1

--5
;WITH nn AS
(SELECT number=0
UNION ALL
SELECT number +1 FROM nn WHERE number<10000
)
SELECT number FROM nn OPTION (maxrecursion 10000)

--6
;WITH nn AS
(SELECT number=0
UNION ALL
SELECT number +1 FROM nn WHERE number<100000
)
SELECT number FROM nn OPTION (maxrecursion 0)

--7
declare @current_year INT
declare @start_year VARCHAR(11)
declare @end_year VARCHAR(11)
SELECT @current_year = DATEPART(YEAR, GETDATE())
SELECT @start_year = CONCAT('01-01-',@current_year)
SELECT @end_year = CONCAT('31-12-',@current_year)

;WITH nn AS(
SELECT CAST(@start_year AS DATE) as cur_day
UNION ALL
SELECT CAST(DATEADD(dd, 1, cur_day) AS DATE) res FROM nn 
WHERE cur_day < CAST(@end_year AS DATE) )
SELECT COUNT (cur_day) FROM nn WHERE DATEPART(WEEKDAY, cur_day) = 1 OR DATEPART(WEEKDAY, cur_day) = 6 
OPTION (maxrecursion 500) 

--8
SELECT DISTINCT(maker) FROM product WHERE type = 'pc' AND maker not IN (
SELECT maker FROM product WHERE type = 'laptop') 

--9
SELECT DISTINCT(maker) FROM product WHERE type = 'pc' AND maker != ALL (
SELECT maker FROM product WHERE type = 'laptop') 

--10
SELECT DISTINCT(maker) FROM product WHERE type = 'pc' AND NOT maker = ANY (
SELECT maker FROM product WHERE type = 'laptop')

--11
SELECT DISTINCT(maker) FROM product WHERE type = 'pc' AND maker IN (
SELECT maker FROM product WHERE type = 'laptop') 

--12
SELECT DISTINCT(maker) FROM product WHERE type = 'pc' AND NOT maker != ALL (
SELECT maker FROM product WHERE type = 'laptop') 

--13
SELECT DISTINCT(maker) FROM product WHERE type = 'pc' AND maker = ANY (
SELECT maker FROM product WHERE type = 'laptop')

--14
SELECT DISTINCT(maker) FROM product WHERE type = 'pc' AND maker NOT IN (
SELECT maker FROM product WHERE type = 'pc' AND NOT maker IN (
SELECT maker FROM pc WHERE product.model = pc.model))
--15
IF EXISTS (SELECT * FROM classes WHERE country = 'Ukraine')
	SELECT class FROM classes WHERE country = 'Ukraine'
ELSE
	SELECT country, class FROM classes

--16
SELECT ship, battle, b.date FROM outcomes o LEFT JOIN battles b ON o.battle=b.name WHERE result = 'damaged'
AND o.ship IN (SELECT ship FROM outcomes o1 LEFT JOIN battles b1 ON o1.battle=b1.name WHERE result = 'ok')

--17
SELECT DISTINCT(maker) FROM product WHERE type = 'pc' AND NOT maker IN (
SELECT maker FROM product WHERE type = 'pc' AND NOT EXISTS (SELECT * FROM PC WHERE product.model = pc.model) )

--18
SELECT TOP 1 maker FROM (SELECT maker,(SELECT MAX(speed) FROM pc WHERE p.model=pc.model) AS speed FROM product p 
WHERE maker IN (SELECT maker FROM product WHERE type = 'printer')) a ORDER BY speed

--19
SELECT class FROM ships WHERE name IN (SELECT o.ship FROM outcomes o WHERE result = 'sunk') OR class IN (
SELECT o.ship FROM outcomes o WHERE result = 'sunk')

--20
SELECT model, price FROM printer WHERE price = (SELECT MAX(price) FROM printer)

--21
SELECT p.type, l.model, speed FROM laptop l INNER JOIN product p ON p.model = l.model 
WHERE speed < (SELECT MIN(speed) FROM pc)

--22
SELECT maker, price FROM printer p INNER JOIN product pr ON p.model = pr.model 
WHERE price = (SELECT MIN(price) FROM (SELECT * FROM printer WHERE color = 'y') a) AND color = 'y'

--23
SELECT o.battle, country FROM outcomes o 
LEFT JOIN ships s ON s.name = o.ship LEFT JOIN classes c ON c.class = s.class
GROUP BY o.battle, country HAVING COUNT(o.ship) >= 2 AND country IS NOT NULL

--24 
SELECT maker, 
(SELECT COUNT(*) FROM pc INNER JOIN product p2 ON pc.model = p2.model WHERE p1.maker = p2.maker) as PC,
(SELECT COUNT(*) FROM laptop l INNER JOIN product p2 ON l.model = p2.model WHERE p1.maker = p2.maker) as laptop,
(SELECT COUNT(*) FROM printer pr INNER JOIN product p2 ON pr.model = p2.model WHERE p1.maker = p2.maker) as printer
FROM product p1 GROUP BY maker

--25 
;WITH CTE_prod (maker, pc) AS (SELECT DISTINCT(maker), 
CASE WHEN type = 'pc' THEN (SELECT COUNT(*) FROM pc WHERE pc.model = product.model) 
ELSE 0 END AS pc
 FROM product)
SELECT maker, CASE WHEN SUM(pc) > 0 THEN 
'yes(' + CAST (SUM(pc) as varchar(10)) + ')' ELSE 'no' END AS PC FROM CTE_prod GROUP BY maker

--26
SELECT i.point, i.date, inc, out FROM income_o i LEFT JOIN outcome_o o ON i.point = o.point AND i.date = o.date
UNION
SELECT o.point, o.date, inc, out FROM income_o i RIGHT JOIN outcome_o o ON i.point = o.point AND i.date = o.date

--27
SELECT s.name, numGuns, bore, displacement, c.type, country, s.launched, s.class FROM classes c 
JOIN ships s ON s.class = c.class WHERE 
CASE WHEN numGuns=8 THEN 1 ELSE 0 END +
CASE WHEN bore=15 THEN 1 ELSE 0 END +
CASE WHEN displacement=32000 THEN 1 ELSE 0 END +
CASE WHEN type='bb' THEN 1 ELSE 0 END +
CASE WHEN country='USA' THEN 1 ELSE 0 END +
CASE WHEN launched=1915 THEN 1 ELSE 0 END +
CASE WHEN s.class='Kongo' THEN 1 ELSE 0 END >=4

--28 ??? WTF ??? 

SELECT o.point, o.date, o.out as out, oo.out as o_out,
CASE WHEN o.out = oo.out THEN 'both'
WHEN o.out > oo.out THEN 'once a day'
ELSE 'more than once a day' END AS res
 FROM outcome o 
FULL JOIN outcome_o oo ON o.point = oo.point AND o.date = oo.date

--29
;WITH CTE_B AS (SELECT * FROM product WHERE maker = 'B')
SELECT maker, cte_b.model, cte_b.type, price FROM CTE_B cte_b INNER JOIN pc ON cte_b.model = pc.model
UNION ALL
SELECT maker, cte_b.model, cte_b.type, price FROM CTE_B cte_b INNER JOIN laptop l ON cte_b.model = l.model
UNION ALL
SELECT maker, cte_b.model, cte_b.type, price FROM CTE_B cte_b INNER JOIN printer p ON cte_b.model = p.model

--30
SELECT name, class FROM ships WHERE name = class
UNION 
SELECT name, name FROM ships WHERE name IN (SELECT class FROM ships)

--31
;WITH CTE_ship AS (SELECT class, name FROM ships
UNION ALL
SELECT ship, class FROM outcomes o LEFT JOIN ships s ON o.ship = s.name
) 
SELECT class, COUNT(name) as cnt FROM CTE_ship GROUP BY class HAVING COUNT(name) = 1

--32
SELECT name FROM ships WHERE launched < 1942
UNION
SELECT ship FROM outcomes WHERE battle = (SELECT name FROM battles b WHERE DATEPART(YEAR, b.date) < 1942)
