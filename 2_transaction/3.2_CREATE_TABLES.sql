USE O_M_module_3
GO

CREATE TABLE film (
	id INT NOT NULL PRIMARY KEY,
	name VARCHAR(40) NOT NULL,
	duration DATETIME NOT NULL,
	year INT NOT NULL,
	country VARCHAR(40) NOT NULL,
	language VARCHAR(20),
	company VARCHAR (40),
	buy_cost INT NOT NULL CHECK(buy_cost>0),
	rent_cost INT NOT NULL CHECK(rent_cost>0),
	description VARCHAR(200),
	inserted_date DATETIME NOT NULL DEFAULT GETDATE(),
	updated_date DATETIME NOT NULL DEFAULT GETDATE(),

	UNIQUE (name, description)
)

CREATE TABLE statistic (
	id INT NOT NULL PRIMARY KEY,
	id_film INT REFERENCES film,
	number_buys INT,
	number_rent INT,
	buy_cost INT,
	rent_cost INT,
	total_buys AS buy_cost*number_buys,
	total_rent AS rent_cost*number_rent,
	last_buys DATETIME,
	last_rent DATETIME,
)