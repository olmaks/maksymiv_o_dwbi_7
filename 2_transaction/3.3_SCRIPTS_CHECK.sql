USE O_M_module_3
GO

--DELETE FROM film  WHERE id = 1
INSERT INTO film(id, name, duration, year, country, buy_cost, rent_cost) 
VALUES(1, 'TIMELESS', 2/22/22, 2011, 'USA', 100, 10)

--DELETE FROM statistic  WHERE id = 1
INSERT INTO statistic(id, number_buys, number_rent) 
VALUES(1, 10, 5)

--UPDATE statistic SET id_film = 1 WHERE id = 1

--Check-constrains. Here we get an error, because buy_cost must be > 0
---- film SET buy_cost = 0
--UPDATE film SET rent_cost = 0

--Check-constrains. Here we get an error, because we have repeated values of film_name - "TIMELESS"
--INSERT INTO film(id, name, duration, year, country, buy_cost, rent_cost) 
--VALUES(2, 'TIMELESS', 3/33/33, 2013, 'ASU', 10, 100)

SELECT * FROM statistic
SELECT * FROM film