USE labor_sql
GO
--1
SELECT ROW_NUMBER() OVER (ORDER BY id_comp, trip_no ASC) AS num, trip_no, id_comp FROM trip

--2
SELECT ROW_NUMBER() OVER (PARTITION BY id_comp ORDER BY id_comp, trip_no ASC) AS num, trip_no, id_comp FROM trip

--3
SELECT model, color, a.type, a.price FROM printer p INNER JOIN 
(SELECT DISTINCT type, MIN (price) OVER (PARTITION BY type) AS price FROM printer) a 
ON a.price = p.price AND a.type = p.type

--4
SELECT maker FROM 
(SELECT maker, COUNT(type) AS cnt FROM product WHERE type = 'pc' GROUP BY maker) a WHERE cnt > 2

--5
SELECT price FROM 
(SELECT price, ROW_NUMBER() OVER(ORDER by price DESC) as rownumber FROM pc) a WHERE rownumber = 2

--6
SELECT name, NTILE(3) OVER (ORDER BY (SUBSTRING(name, CHARINDEX(' ', name), len(name)))) FROM passenger

--7
;WITH CTE_pag AS (SELECT *, ROW_NUMBER() OVER (ORDER BY price DESC) AS id FROM PC)
SELECT *, COUNT(id) OVER() AS row_total, (id + 2) / 3 AS page_num, MAX((id + 2) / 3) OVER() AS page_total 
FROM CTE_pag

--8
SELECT TOP 1 MAX(max_sum) OVER(ORDER BY max_sum DESC) AS max_sum, type = 'out', date, point FROM (
SELECT TOP 1 MAX(out) OVER(ORDER BY out DESC) AS max_sum, type = 'out', date, point FROM outcome
UNION ALL
SELECT TOP 1 MAX(out) OVER(ORDER BY out DESC) AS max_sum, type = 'out', date, point FROM outcome_o
UNION ALL
SELECT TOP 1 MAX(inc) OVER(ORDER BY inc DESC) AS max_sum, type = 'inc', date, point FROM income
UNION ALL
SELECT TOP 1 MAX(inc) OVER(ORDER BY inc DESC) AS max_sum, type = 'inc', date, point FROM income_o
) res

--9
SELECT *, AVG(price) OVER(PARTITION BY speed) - price AS dif_local_price, 
price - AVG(price) OVER() AS dif_total_price,
AVG(price) OVER() AS total_price FROM PC