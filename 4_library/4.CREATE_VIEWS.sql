USE O_Maksymiv_Library_view
GO

CREATE OR ALTER VIEW Authors_view
AS
SELECT * FROM O_MAKSYMIV.dbo.Authors
GO

CREATE OR ALTER VIEW Publishers_view
AS
SELECT * FROM O_MAKSYMIV.dbo.Publishers
GO

CREATE OR ALTER VIEW Books_view
AS
SELECT * FROM O_MAKSYMIV.dbo.Books
GO

CREATE OR ALTER VIEW BooksAuthors_view
AS
SELECT * FROM O_MAKSYMIV.dbo.BooksAuthors
GO

CREATE OR ALTER VIEW Authors_log_view
AS
SELECT * FROM O_MAKSYMIV.dbo.Authors_log
GO