USE O_MAKSYMIV
GO

CREATE OR ALTER TRIGGER author_insert
ON Authors
AFTER Insert
AS
BEGIN
	INSERT INTO Authors_log(Author_Id_new, Name_new, URL_new) 
	SELECT inserted.Author_Id, inserted.Name, inserted.URL
	FROM Authors INNER JOIN inserted ON Authors.Author_id = inserted.Author_id
	INSERT INTO Authors_log(operation_type) VALUES ('I')
	INSERT INTO Authors_log(Author_Id_old, Name_old, URL_old) VALUES (NULL, NULL, NULL)
END
GO

CREATE OR ALTER TRIGGER author_delete
ON Authors
AFTER DELETE
AS
BEGIN
	INSERT INTO Authors_log(Author_Id_old, Name_old, URL_old) 
	SELECT inserted.Author_Id, inserted.Name, inserted.URL
	FROM Authors INNER JOIN inserted ON Authors.Author_id = inserted.Author_id
	INSERT INTO Authors_log(operation_type) VALUES ('D')
	INSERT INTO Authors_log(Author_Id_new, Name_new, URL_new) VALUES (NULL, NULL, NULL)
END
GO

CREATE OR ALTER TRIGGER author_log_update
ON Authors
AFTER UPDATE
AS
INSERT INTO Authors_log(operation_type) VALUES ('U')
