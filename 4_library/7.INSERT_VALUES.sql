USE O_MAKSYMIV
GO

INSERT INTO Authors(Author_id, Name, URL) VALUES
	(1, 'Author1', 'www.author1.com'),
	(2, 'Author2', 'www.author2.com'),
	(3, 'Author3', 'www.author3.com'),
	(4, 'Author4', 'www.author4.com'),
	(5, 'Author5', 'www.author5.com'),
	(6, 'Author6', 'www.author6.com'),
	(7, 'Author7', 'www.author7.com'),
	(8, 'Author8', 'www.author8.com'),
	(9, 'Author9', 'www.author9.com'),
	(10, 'Author10', 'www.author10.com'),
	(11, 'Author11', 'www.author11.com'),
	(12, 'Author12', 'www.author12.com'),
	(13, 'Author13', 'www.author13.com'),
	(14, 'Author14', 'www.author14.com'),
	(15, 'Author15', 'www.author15.com'),
	(16, 'Author16', 'www.author16.com'),
	(17, 'Author17', 'www.author17.com'),
	(18, 'Author18', 'www.author18.com'),
	(19, 'Author19', 'www.author19.com'),
	(20, 'Author20', 'www.author20.com');

INSERT INTO Publishers(Publisher_id, Name) VALUES
	(1, 'Publisher1'),
	(2, 'Publisher2'),
	(3, 'Publisher3'),
	(4, 'Publisher4'),
	(5, 'Publisher5'),
	(6, 'Publisher6'),
	(7, 'Publisher7'),
	(8, 'Publisher8'),
	(9, 'Publisher9'),
	(10, 'Publisher10'),
	(11, 'Publisher11'),
	(12, 'Publisher12'),
	(13, 'Publisher13'),
	(14, 'Publisher14'),
	(15, 'Publisher15'),
	(16, 'Publisher16'),
	(17, 'Publisher17'),
	(18, 'Publisher18'),
	(19, 'Publisher19'),
	(20, 'Publisher20');

INSERT INTO Books(ISBN, Publisher_id, URL) VALUES
	('978-3-16-148410-0', 1, 'Publisher1'),
	('978-3-16-148410-1', 2, 'Publisher2'),
	('978-3-16-148410-2', 3, 'Publisher3'),
	('978-3-16-148410-3', 4, 'Publisher4'),
	('978-3-16-148410-4', 5, 'Publisher5'),
	('978-3-16-148410-5', 6, 'Publisher6'),
	('978-3-16-148410-6', 7, 'Publisher7'),
	('978-3-16-148410-7', 8, 'Publisher8'),
	('978-3-16-148410-8', 10, 'Publisher9'),
	('978-3-16-148410-9', 11, 'Publisher10'),
	('978-3-16-148410-10', 11, 'Publisher11'),
	('978-3-16-148410-11', 11, 'Publisher12'),
	('978-3-16-148410-12', 11, 'Publisher13'),
	('978-3-16-148410-13', 11, 'Publisher14'),
	('978-3-16-148410-14', 11, 'Publisher15'),
	('978-3-16-148410-15', 11, 'Publisher16'),
	('978-3-16-148410-16', 11, 'Publisher17'),
	('978-3-16-148410-17', 11, 'Publisher18'),
	('978-3-16-148410-18', 11, 'Publisher19'),
	('978-3-16-148410-19', 11, 'Publisher20');

INSERT INTO BooksAuthors(ISBN, BooksAuthors_id, Author_id) VALUES
	('978-3-16-148410-0', 1, 1),
	('978-3-16-148410-1', 2, 2),
	('978-3-16-148410-2', 3, 3),
	('978-3-16-148410-3', 4, 4),
	('978-3-16-148410-4', 5, 5),
	('978-3-16-148410-5', 6, 6),
	('978-3-16-148410-6', 7, 7),
	('978-3-16-148410-7', 8, 8),
	('978-3-16-148410-8', 10, 10),
	('978-3-16-148410-9', 11, 11),
	('978-3-16-148410-10', 12, 12),
	('978-3-16-148410-11', 13, 13),
	('978-3-16-148410-12', 14, 13),
	('978-3-16-148410-13', 15, 14),
	('978-3-16-148410-14', 16, 15),
	('978-3-16-148410-15', 17, 16),
	('978-3-16-148410-16', 18, 17),
	('978-3-16-148410-17', 19, 18),
	('978-3-16-148410-18', 20, 19),
	('978-3-16-148410-19', 21, 20);

SELECT * FROM Authors
SELECT * FROM Books
SELECT * FROM BooksAuthors
SELECT * FROM Publishers
SELECT * FROM Authors_log