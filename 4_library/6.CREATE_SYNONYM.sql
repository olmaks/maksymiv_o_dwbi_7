USE O_Maksymiv_Library_view
GO

CREATE SYNONYM dbo.Authors_synonym for O_MAKSYMIV.dbo.Authors
CREATE SYNONYM dbo.Publishers_synonym for O_MAKSYMIV.dbo.Publishers
CREATE SYNONYM dbo.Books_synonym for O_MAKSYMIV.dbo.Books
CREATE SYNONYM dbo.BooksAuthors_synonym for O_MAKSYMIV.dbo.BooksAuthors
CREATE SYNONYM dbo.Authors_log_synonym for O_MAKSYMIV.dbo.Authors_log

