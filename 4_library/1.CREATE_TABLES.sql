USE O_MAKSYMIV
GO

DROP TABLE IF EXISTS Authors
GO
CREATE TABLE Authors (
	Author_id INT NOT NULL PRIMARY KEY,
	Name varchar(40) NOT NULL UNIQUE,
	URL varchar(40) NOT NULL DEFAULT 'www.author_name.com',
	inserted date NOT NULL DEFAULT getdate(),
	inserted_by varchar(40) NOT NULL DEFAULT system_user,
	updated date,
	updated_by varchar(40)
)

DROP TABLE IF EXISTS Publishers
GO
CREATE TABLE Publishers (
	Publisher_id INT NOT NULL PRIMARY KEY,
	Name varchar(40) NOT NULL UNIQUE,
	URL varchar(40) NOT NULL DEFAULT 'www.publisher.name.com',
	inserted date NOT NULL DEFAULT getdate(),
	inserted_by varchar(40) NOT NULL DEFAULT system_user,
	updated date,
	updated_by varchar(40)
)

DROP TABLE IF EXISTS Authors_log
GO
CREATE TABLE Authors_log (
	operation_id INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	Author_Id_new INT,
	Name_new varchar(40),
	URL_new varchar(40),
	Author_Id_old INT,
	Name_old varchar(40),
	URL_old varchar(40),
	operation_type varchar(40) NOT NULL CHECK(operation_type IN ('I','D','U')) DEFAULT 'I',
	operation_datetime date NOT NULL DEFAULT getdate()
)

DROP TABLE IF EXISTS Books
GO
CREATE TABLE Books (
	ISBN varchar(40) NOT NULL PRIMARY KEY,
	Publisher_id INT NOT NULL FOREIGN KEY REFERENCES Publishers(Publisher_id),
	URL varchar(40) NOT NULL UNIQUE,
	Price INT NOT NULL DEFAULT 0 CHECK(Price >= 0),
	inserted date NOT NULL DEFAULT getdate(),
	inserted_by varchar(40) NOT NULL DEFAULT system_user,
	updated date,
	updated_by varchar(40)
)

DROP TABLE IF EXISTS BooksAuthors
GO
CREATE TABLE BooksAuthors (
	BooksAuthors_id INT NOT NULL PRIMARY KEY DEFAULT 1 CHECK(BooksAuthors_id >= 1),
	ISBN varchar(40) NOT NULL FOREIGN KEY REFERENCES Books(ISBN),
	Author_id INT NOT NULL FOREIGN KEY REFERENCES Authors(Author_id),
	Seq_No INT NOT NULL DEFAULT 1 CHECK(Seq_No >= 1),
	inserted date NOT NULL DEFAULT getdate(),
	inserted_by varchar(40) NOT NULL DEFAULT system_user,
	updated date,
	updated_by varchar(40)
)