USE O_MAKSYMIV
GO

CREATE OR ALTER TRIGGER prevent_delete
ON Authors_log
AFTER DELETE
AS
BEGIN
	PRINT 'You cannot delete something here!'
	ROLLBACK TRANSACTION
END
GO

--DELETE FROM Authors_log WHERE operation_id = 1
--SELECT * FROM Authors_log