USE O_MAKSYMIV
GO

UPDATE Authors SET Name = 'Author21', URL = 'www.author21.com' WHERE Author_id = 1 
UPDATE Authors SET Name = 'Author22', URL = 'www.author22.com' WHERE Author_id = 2
UPDATE Authors SET Name = 'Author23', URL = 'www.author23.com' WHERE Author_id = 3 
UPDATE Authors SET Name = 'Author24', URL = 'www.author24.com' WHERE Author_id = 4 
UPDATE Authors SET Name = 'Author25', URL = 'www.author25.com' WHERE Author_id = 5 
UPDATE Authors SET Name = 'Author26', URL = 'www.author26.com' WHERE Author_id = 6 
UPDATE Authors SET Name = 'Author27', URL = 'www.author27.com' WHERE Author_id = 7 
UPDATE Authors SET Name = 'Author28', URL = 'www.author28.com' WHERE Author_id = 8 
UPDATE Authors SET Name = 'Author29', URL = 'www.author29.com' WHERE Author_id = 9 
UPDATE Authors SET Name = 'Author30', URL = 'www.author30.com' WHERE Author_id = 10 

UPDATE Publishers SET Name = 'Publisher31' WHERE Publisher_id = 10
UPDATE Publishers SET Name = 'Publisher32' WHERE Publisher_id = 11
UPDATE Publishers SET Name = 'Publisher22' WHERE Publisher_id = 12
UPDATE Publishers SET Name = 'Publisher23' WHERE Publisher_id = 13
UPDATE Publishers SET Name = 'Publisher24' WHERE Publisher_id = 14
UPDATE Publishers SET Name = 'Publisher25' WHERE Publisher_id = 15
UPDATE Publishers SET Name = 'Publisher26' WHERE Publisher_id = 16
UPDATE Publishers SET Name = 'Publisher27' WHERE Publisher_id = 17
UPDATE Publishers SET Name = 'Publisher28' WHERE Publisher_id = 18
UPDATE Publishers SET Name = 'Publisher29' WHERE Publisher_id = 19

UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 0
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 1
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 2
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 3
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 4
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 5
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 6
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 7
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 8
UPDATE Books SET URL = 'http://www.' + url + '.com' WHERE Publisher_id = 9

UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 51 WHERE Author_id = 10
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 22 WHERE Author_id = 11
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 23 WHERE Author_id = 12
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 24 WHERE Author_id = 13
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 25 WHERE Author_id = 14
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 26 WHERE Author_id = 15
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 27 WHERE Author_id = 16
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 28 WHERE Author_id = 17
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 29 WHERE Author_id = 18
UPDATE BooksAuthors SET BooksAuthors_id = BooksAuthors_id + 30 WHERE Author_id = 19

SELECT * FROM Authors
SELECT * FROM Books
SELECT * FROM BooksAuthors
SELECT * FROM Publishers
SELECT * FROM Authors_log