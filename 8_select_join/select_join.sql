use [labor_sql];
go


--1
SELECT  p.maker, p.type, pc.speed, pc.hd FROM [pc] pc, [product] p WHERE hd <= 8 AND pc.model = p.model
SELECT p.maker, p.type, pc.speed FROM product p INNER JOIN pc ON p.model = pc.model AND pc.hd <= 8 
--2
SELECT  p.maker FROM [pc] pc, [product] p WHERE speed >= 600 AND pc.model = p.model
SELECT p.maker FROM product p INNER JOIN pc ON p.model = pc.model AND pc.speed >= 600 
--3
SELECT DISTINCT p.maker FROM product p, laptop l WHERE speed <= 500 AND p.model = l.model
SELECT DISTINCT p.maker FROM product p INNER JOIN laptop l ON speed <= 500 AND p.model = l.model
--4
SELECT DISTINCT a.code, b.code, a.ram, a.hd FROM laptop a, laptop b WHERE a.hd = b.hd AND a.ram = b.ram AND a.code != b.code AND a.code>=b.code
--SELECT a.code, b.code, a.ram, b.ram, a.hd, b.hd FROM laptop a JOIN laptop b ON a.ram = b.ram AND a.hd = b.hd AND a.code != b.code 
--5
SELECT c1.country, c1.type, c2.type FROM classes c1, classes c2 WHERE (c1.type = 'bb' AND c2.type = 'bc') AND c1.country = c2.country
--6
SELECT pc.model, p.maker FROM pc, product p WHERE pc.price <= 600 AND pc.model = p.model
--7
SELECT pr.model, p.maker FROM printer pr, product p WHERE pr.price >= 300 AND pr.model = p.model
--8 NOT
SELECT p.maker, pc.model, pc.price FROM pc, product p WHERE p.model = pc.model
--9
SELECT p.maker, p.model, pc.price FROM product p, pc WHERE p.model = pc.model
--10
SELECT p.maker, p.type, l.model, l.speed from laptop l, product p WHERE p.model = l.model AND l.speed > 600
--11
SELECT DISTINCT s.class, displacement FROM Ships s, Classes c WHERE s.class = c.class
--12
SELECT b.name, b.date FROM Outcomes o, battles b WHERE result = 'OK' AND o.battle = b.name
--13
SELECT s.name, c.country FROM Ships s, classes c WHERE c.class = s.class
--14
SELECT t.plane, c.name FROM trip t, company c WHERE t.id_comp = c.id_comp AND t.plane = 'Boeing'
--15
SELECT p.id_psg, t.date FROM passenger p, pass_in_trip t WHERE p.id_psg = t.id_psg
--16
SELECT pc.model, speed, hd FROM pc, product p WHERE (hd = 10 OR hd = 20) AND (p.model = pc.model AND p.maker = 'A')
--17
SELECT * FROM product pivot (COUNT(model) FOR type in ([pc], [laptop], [printer])
) as pvt
--18
SELECT AVG([11]) [11], AVG([12]) [12], AVG([13]) [13], AVG([14]) [14], AVG([15]) [15] FROM laptop 
PIVOT (AVG(PRICE) FOR SCREEN IN ([11], [12], [13], [14], [15])) AS pvt_table
--19
SELECT * FROM laptop CROSS APPLY (SELECT p.maker FROM product p WHERE p.model = laptop.model) M
--20

--return max price for maker
--SELECT a.maker, MAX(a.price) price FROM (
--SELECT * FROM product p CROSS APPLY (SELECT l.price FROM laptop l WHERE l.model = p.model) M) a
--GROUP BY maker 

SELECT code, model, speed, ram, hd, price, screen, max_price 
FROM laptop l CROSS APPLY (SELECT p.maker FROM product p WHERE l.model = p.model) M
LEFT JOIN (SELECT a.maker, MAX(a.price) AS max_price FROM (
SELECT * FROM product p CROSS APPLY (SELECT l.price FROM laptop l WHERE l.model = p.model) M) a
GROUP BY maker ) table2 ON M.maker = table2.maker

--21
SELECT * FROM laptop a CROSS APPLY (SELECT TOP 1 * FROM laptop b WHERE 
code != 1 AND (a.model < b.model OR (a.code < b.code AND a.model = b.model))
ORDER BY b.model, b.code) m ORDER BY a.model, a.code

--22
SELECT * FROM laptop a OUTER APPLY (SELECT TOP 1 * FROM laptop b WHERE 
code != 1 AND (a.model < b.model OR (a.code < b.code AND a.model = b.model))
ORDER BY b.model, b.code) m ORDER BY a.model, a.code

--23
SELECT * FROM (SELECT DISTINCT type FROM product) p1 
CROSS APPLY(SELECT TOP 3 * FROM product p2 WHERE p1.type = p2.type) m

--24
SELECT code, name, value FROM laptop CROSS APPLY (VALUES ('speed', speed),
	('ram', ram),
	('hd', hd),
	('screen', screen) ) a (name, value)